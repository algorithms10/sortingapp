package com.amaca.sorting.sortingapp

import com.amaca.sorting.sortingapp.algorithms.countingSort
import com.amaca.sorting.sortingapp.algorithms.heapSort
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import kotlin.random.Random
import kotlin.collections.ArrayList


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class AlgorithmsTest {

    val arraySize = 1000000
//    val arraySize = 5
    val maxInt = 1000000
    lateinit var inputArray: ArrayList<Int>
    @Before
    fun setup(){
        inputArray = ArrayList(List(arraySize) { Random.nextInt(0, maxInt) })

    }

    @Test
    fun testHeapSortingNumberArray(){
        val inputCopy: ArrayList<Int> = inputArray.clone() as ArrayList<Int>
        inputArray.heapSort()
        inputCopy.sort()
        for (index in 0 until inputArray.size){
            assertEquals(inputArray[index], inputCopy[index])
        }
    }

    @Test
    fun testCountingSortingNumberArray(){
        val inputCopy: ArrayList<Int> = inputArray.clone() as ArrayList<Int>
        inputArray.countingSort()
        inputCopy.sort()
        for (index in 0 until inputArray.size){
            assertEquals(inputArray[index], inputCopy[index])
        }
    }

}