package com.amaca.sorting.sortingapp.algorithms

fun ArrayList<Int>.heapSort(){
    buildMaxHeap()
    var heap = this.size - 1
    for (index in (this.size - 1) downTo 1  ){
        swap(0, index)
        heap--
        maxHeapify(0, heap)
    }
}
fun ArrayList<Int>.buildMaxHeap(){
    val heapSize = this.size - 1
    for (index in (this.size/2 downTo  0) ){
        maxHeapify(index, heapSize)
    }
}

fun ArrayList<Int>.maxHeapify(index: Int, heapSize: Int){
    val leftIndex = 2 * index + 1
    val rightIndex = 2 * index + 2

    val currentValue = this[index]

    var maxValueIndex = if (leftIndex<= heapSize && currentValue<this[leftIndex]){
        leftIndex
    }else
        index

    if (rightIndex<= heapSize && this[maxValueIndex]<this[rightIndex]){
        maxValueIndex = rightIndex
    }

    if (maxValueIndex != index){
        swap(index, maxValueIndex)
        maxHeapify(maxValueIndex, heapSize)
    }
}

fun ArrayList<Int>.swap(index: Int, index2:Int){ // Dev responsibility to check range.

    val indexVal = this[index]
    this[index] = this[index2]
    this[index2] = indexVal

}