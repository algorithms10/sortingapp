package com.amaca.sorting.sortingapp.algorithms

fun ArrayList<Int>.countingSort(){
    val maxValue = this.maxOrNull()
    maxValue?.let {
        val elementCounter = IntArray(it+1)
//        for (index in 0 until this.size){
        for(element in this)
            ++elementCounter[element]


        for (index in 1 until elementCounter.size) {
            elementCounter[index] += elementCounter[index - 1 ]
        }
        val answerArray = IntArray(this.size)

        for (element in this){
            answerArray[elementCounter[element]-1] = element
            --elementCounter[element]
        }
        print(answerArray)
        this.clear()
        this.addAll(ArrayList(answerArray.toList()))
    }
}